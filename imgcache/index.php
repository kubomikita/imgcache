<?php
error_reporting(0);
define("PATH", "../");
define("HASH", false);
include("resize.class.php");

function errorImage($url,$width,$height){
	$notfound=PATH.$url; //"images/not-found.png"
	list($name,$ext)=explode(".",$url);
	$new=PATH.$name."__{$width}-{$height}-6.".$ext;

	$resizeObj=new resize($notfound);
	$resizeObj->resizeImage($height, $width, 'scale');
	$temp=$resizeObj->saveImage($new);

	header("Content-type: image/jpeg");
	readfile($temp);
	exit;
}
function encode_url($img){
	return str_replace("/","___",$img);
}
function decode_url($img){
	return str_replace("___","/",$img);
}

$image=$_GET["i"];

//var_dump($link,$image); exit;
if(HASH){
	$link=PATH.$image;
	$tmp="temp/".sha1($link);
} else {
	$link=$image;
	$tmp="temp/".encode_url($image);
}
$extension=substr($image,-4);
switch ($extension) {
	case '.png':
		$mime="image/png";
	break;
	case '.jpg':
		$mime="image/jpeg";
	break;
	case '.gif':
		$mime="image/gif";
	break;
}

//var_dump($tmp);exit();
if(file_exists($tmp)){
	//var_dump($tmp);exit;
	header("Content-type: $mime");
	readfile($tmp);
	exit;
} else {
        $scan = preg_match("/(.+)_(([0-9]+)-([0-9]+)[-]?([1-9]?)[-]?([a-zA-Z0-9]*))(\.jpg|\.gif|\.png)/i", $image ,$matches);
    //var_dump($image, $matches);
        list($full,$name,$params,$width,$height,$method,$color,$extension) = $matches;
        //var_dump($matches,$color);
    //exit;
        if($method == "") $method = 2;
        //var_dump($method);exit;
        $methods=array(1=>"exact", "portrait", "landscape", "auto", "crop","scale");
	/*list($name, $params)=explode("__", substr($image,0,-4));
	$methods=array(1=>"exact", "portrait", "landscape", "auto", "crop","scale");
	$param=explode("-", $params);
	$height=$param[0];
	$width=$param[1];*/
	if($scan){
		if($height > 5 and $width > 5){
                    //var_dump(PATH.$name.$extension,file_exists(PATH.$name.".".$extension));exit;
			if(file_exists(PATH.$name.$extension)){
				$resizeObj = new resize(PATH.$name.$extension);
				$resizeObj->resizeImage($height, $width, $methods[$method], $color);
				$temp=$resizeObj->saveImage($link,HASH);
				header("Content-type: $mime");
				readfile($temp);
				exit;
			} else {
				errorImage("images/not-found.jpg",$width,$height);
				exit;
			}
		} else {
			errorImage("images/bad-size.png",100,100);
			exit;
		}
	} else {
		errorImage("images/bad-size.png",100,100);
		exit;
	}
}
?>